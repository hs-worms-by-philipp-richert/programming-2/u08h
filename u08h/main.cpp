// Aufgabe u08h
// Operatoren-Implementierungen fuer eigene Klassen
// Philipp Richert
// 01.12.2022

#include "cKompRat.h"

int main() {
	cKompRat brueche[4] = {
		cKompRat({3, 6}, {21, -7}),
		cKompRat({8, -10}, {-4, 3}),
		cKompRat({-8, 13}, {4, 5}),
		cKompRat({21, 37})
	};

	for (cKompRat item : brueche) {
		item.ausgabe();
	}

	cout << endl;

	cKompRat addTest = brueche[0] + brueche[1];
	cKompRat subtTest = brueche[0] - brueche[1];
	cKompRat divTest = brueche[2] / brueche[3];
	cKompRat multTest = brueche[2] * brueche[3];

	addTest.ausgabe();
	subtTest.ausgabe();
	divTest.ausgabe();
	multTest.ausgabe();

	cout << endl;

	for (cKompRat item : brueche) {
		item.ausgabe();
	}

	return 0;
}