#pragma once

#include <iostream>
//#include <vector>

using namespace std;

//typedef vector<cBruch> bruchArr;

class cBruch
{
private:
	int zaehler;
	int nenner;
	friend int ggT(int, int);
	void kuerzen();
	friend cBruch& add(const cBruch&, const cBruch&);
	friend cBruch& subt(const cBruch&, const cBruch&);
	friend cBruch& mult(const cBruch&, const cBruch&);
	friend cBruch& div(const cBruch&, const cBruch&);
	friend int vergleich(const cBruch&, const cBruch&);
	friend void tausch(cBruch&, cBruch&);
	friend void sortier(cBruch[], int);
	friend ostream& operator << (ostream&, const cBruch&);
public:
	cBruch(int = 0, int = 1);
	cBruch(const cBruch&);
	void ausgabe();
	double getDouble() const;
};
