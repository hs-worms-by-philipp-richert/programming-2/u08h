#include "cKompRat.h"

cKompRat::cKompRat(cBruch a_in, cBruch b_in) : re(a_in), im(b_in) {
}

// copy constructor
cKompRat::cKompRat(const cKompRat& c) : re(c.re), im(c.im) {
}

// Ordnen zweier komplexer Zahlen (mathematisch nicht moeglich)
// mathematisch nicht korrekt, fuer diese Aufgabe hinreichend
int kompRatVergleich(const cKompRat& a, const cKompRat& b) {
	double dA = sqrt(pow(a.re.getDouble(), 2) + pow(a.im.getDouble(), 2));
	double dB = sqrt(pow(b.re.getDouble(), 2) + pow(b.im.getDouble(), 2));

	if (dA > dB) return 1;
	else if (dA == dB) return 0;
	else if (dA < dB) return -1;
}

// output stream operator for cKompRat objects
ostream& operator << (ostream& os, const cKompRat& a) {
	os << a.re << ((a.im.getDouble() < 0) ? " " : " +") << a.im << "i";
	return os;
}

// add operator for cKompRat objects
cKompRat& operator + (const cKompRat& a, const cKompRat& b) {
	cBruch tmpRe = add(a.re, b.re);
	cBruch tmpIm = add(a.im, b.im);
	static cKompRat tmpKomp = cKompRat(tmpRe, tmpIm);

	return tmpKomp;
}

// subtraction operator for cKompRat objects
cKompRat& operator - (const cKompRat& a, const cKompRat& b) {
	cBruch tmpRe = subt(a.re, b.re);
	cBruch tmpIm = subt(a.im, b.im);
	static cKompRat tmpKomp = cKompRat(tmpRe, tmpIm);

	return tmpKomp;
}

// division operator for cKompRat objects
cKompRat& operator / (const cKompRat& a, const cKompRat& b) {
	cBruch tmpRe = div(a.re, b.re);
	cBruch tmpIm = div(a.im, b.im);
	static cKompRat tmpKomp = cKompRat(tmpRe, tmpIm);

	return tmpKomp;
}

// multiplication operator for cKompRat objects
cKompRat& operator * (const cKompRat& a, const cKompRat& b) {
	cBruch tmpRe = mult(a.re, b.re);
	cBruch tmpIm = mult(a.im, b.im);
	static cKompRat tmpKomp = cKompRat(tmpRe, tmpIm);

	return tmpKomp;
}

// less-than operator
bool operator < (const cKompRat& a, const cKompRat& b) {
	return kompRatVergleich(a, b) == -1;
}

// equal-as operator
bool operator == (const cKompRat& a, const cKompRat& b) {
	return kompRatVergleich(a, b) == 0;
}

// more-than operator
bool operator > (const cKompRat& a, const cKompRat& b) {
	return kompRatVergleich(a, b) == 1;
}

// overwrite NOT operator with swapping real and imaginary portions
cKompRat& operator ~ (const cKompRat& a) {
	static cKompRat tmpKomp = cKompRat(a.im, a.re);

	return tmpKomp;
}

// output method
void cKompRat::ausgabe() {
	cout << "Komplexe Zahl:" << endl << "\t" << re << ((im.getDouble() < 0) ? " " : " +") << im << "i"
		<< " = " << re.getDouble() << ((im.getDouble() < 0) ? " " : " +") << im.getDouble() << "i"
		<< endl;
}