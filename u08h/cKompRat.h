#pragma once

#include "cBruch.h"
#include <cmath>

using namespace std;

class cKompRat :
    public cBruch
{
private:
    cBruch re;
    cBruch im;
    friend int kompRatVergleich(const cKompRat&, const cKompRat&);
    friend ostream& operator << (ostream&, const cKompRat&);
    friend cKompRat& operator + (const cKompRat&, const cKompRat&);
    friend cKompRat& operator - (const cKompRat&, const cKompRat&);
    friend cKompRat& operator / (const cKompRat&, const cKompRat&);
    friend cKompRat& operator * (const cKompRat&, const cKompRat&);
    friend bool operator < (const cKompRat&, const cKompRat&);
    friend bool operator == (const cKompRat&, const cKompRat&);
    friend bool operator > (const cKompRat&, const cKompRat&);
    friend cKompRat& operator ~ (const cKompRat&);
public:
    cKompRat(cBruch = cBruch(), cBruch = cBruch());   // cBruch hat bereits als Vorgabewert 0/1
    cKompRat(const cKompRat&);
    void ausgabe();
};

