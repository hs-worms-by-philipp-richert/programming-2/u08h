#include "cBruch.h"

cBruch::cBruch(int zaehl_in, int nenn_in) {
	zaehler = (nenn_in < 0) ? zaehl_in * -1 : zaehl_in;
	nenner = (nenn_in == 0) ? 1 : (nenn_in < 0) ? nenn_in * -1 : nenn_in;

	kuerzen();
}

// copy-constructor
cBruch::cBruch(const cBruch& cpyBruch) {
	zaehler = cpyBruch.zaehler;
	nenner = cpyBruch.nenner;
}

// find greatest common divider 
int ggT(int x, int y) {
	if (x == y) {
		return x;
	}
	else {
		if (x > y) {
			return ggT(x - y, y);
		}
		else {
			return ggT(x, y - x);
		}
	}
}

// reduce fraction
void cBruch::kuerzen() {
	if (zaehler == 0) {
		nenner = 1;
		return;
	}

	int ggt = ggT(zaehler >= 0 ? zaehler : zaehler * -1, nenner);

	zaehler /= ggt;
	nenner /= ggt;
}

// output method
void cBruch::ausgabe() {
	cout << "Bruch: " << zaehler << "/" << nenner << " = " << (double)zaehler/(double)nenner << endl;
}

// getter method for fraction as double-value
double cBruch::getDouble() const {
	return (double)zaehler / (double)nenner;
}

// Friend-methods
// addition
cBruch& add(const cBruch& a, const cBruch& b) {
	static cBruch tmpBruch;

	tmpBruch.zaehler = a.zaehler * b.nenner + b.zaehler * a.nenner;
	tmpBruch.nenner = a.nenner * b.nenner;

	tmpBruch.kuerzen();

	return tmpBruch;
}

// subtraction
cBruch& subt(const cBruch& a, const cBruch& b) {
	static cBruch tmpBruch;

	tmpBruch.zaehler = a.zaehler * b.nenner - b.zaehler * a.nenner;
	tmpBruch.nenner = a.nenner * b.nenner;

	tmpBruch.kuerzen();

	return tmpBruch;
}

// multiplication
cBruch& mult(const cBruch& a, const cBruch& b) {
	static cBruch tmpBruch;

	tmpBruch.zaehler = a.zaehler * b.zaehler;
	tmpBruch.nenner = a.nenner * b.nenner;

	tmpBruch.kuerzen();

	return tmpBruch;
}

// division
cBruch& div(const cBruch& a, const cBruch& b) {
	static cBruch tmpBruch;

	if (b.zaehler == 0) {
		cout << "Achtung! Nulldivision erkannt, folglich wurde nicht dividiert" << endl;
		tmpBruch.zaehler = a.zaehler;
		tmpBruch.nenner = a.nenner;
		return tmpBruch;
	}

	tmpBruch.zaehler = a.zaehler * b.nenner;
	tmpBruch.nenner = a.nenner * b.zaehler;

	tmpBruch.kuerzen();

	return tmpBruch;
}

// output stream operator implementation
ostream& operator << (ostream& os, const cBruch& bruch) {
	os << bruch.zaehler << "/" << bruch.nenner/* << " = " << (double)bruch.zaehler / (double)bruch.nenner*/;
	return os;
}

int vergleich(const cBruch& a, const cBruch& b) {
	double alpha = (double)a.zaehler / (double)a.nenner;
	double beta = (double)b.zaehler / (double)b.nenner;

	if (alpha < beta) return -1;
	else if (alpha == beta) return 0;
	else if (alpha > beta) return 1;
}

// swap numerator and denominator
void tausch(cBruch& a, cBruch& b) {
	cBruch tmpBruch;

	tmpBruch.zaehler = b.zaehler;
	tmpBruch.nenner = b.nenner;

	b.zaehler = a.zaehler;
	b.nenner = a.nenner;

	a.zaehler = tmpBruch.zaehler;
	a.nenner = tmpBruch.nenner;
}

// sort array of fractions
void sortier(cBruch arrBruch[], int arrSize) {
	// Bubble Sort Algorithmus (unoptimiert) bei ANZ Objekten
	for (int n = arrSize; n > 1; n--) {
		for (int i = 0; i < n - 1; i++) {
			if (vergleich(arrBruch[i], arrBruch[i + 1]) == 1) { // Elemente vergleichen
				tausch(arrBruch[i], arrBruch[i + 1]); // Elemente tauschen
			}
		}
	}
}